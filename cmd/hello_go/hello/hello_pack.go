package hello

import "fmt"

func Hello(x string) {
	fmt.Printf("Hello from Hello(" + x + ")\n")
}
