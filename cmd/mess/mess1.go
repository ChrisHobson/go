package main

import "fmt"
import "sync"

func main() {
	var wg sync.WaitGroup
	for i := 1; i < 10; i += 1 {
		wg.Add(1)
		go print(&wg, i)
	}
	fmt.Printf("hello mess1\n")
	wg.Wait()
}

func print(wg *sync.WaitGroup, i int) {
	defer wg.Done()
	fmt.Printf("Hello from print(%d)\n", i)
}
